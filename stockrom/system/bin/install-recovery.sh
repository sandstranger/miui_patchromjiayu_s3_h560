#!/system/bin/sh
if [ -f /system/etc/recovery-transform.sh ]; then
  exec sh /system/etc/recovery-transform.sh 12134400 41e9d5571204db121ef2de8fdd60f51f3c9292de 8853504 e36a274d3a7809ff61822caf04441afb14a8555c
fi

if ! applypatch -c EMMC:/dev/block/platform/mtk-msdc.0/by-name/recovery:12134400:41e9d5571204db121ef2de8fdd60f51f3c9292de; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/mtk-msdc.0/by-name/boot:8853504:e36a274d3a7809ff61822caf04441afb14a8555c EMMC:/dev/block/platform/mtk-msdc.0/by-name/recovery 41e9d5571204db121ef2de8fdd60f51f3c9292de 12134400 e36a274d3a7809ff61822caf04441afb14a8555c:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
